
/**********************************************************************************
*                                                                                 *
*     Gferasure was developed by the Center for Research in Storage Systems       *
*                         http://www.crss.ucsc.edu/                               *
*                                                                                 *
* BSD 3-Clause License                                                            *
*                                                                                 *
* Copyright (c) 2016, Regents of the University of California                     *
*                                                                                 *
* All rights reserved.                                                            *
*                                                                                 *
* Redistribution and use in source and binary forms, with or without              *
* modification, are permitted provided that the following conditions are met:     *
*                                                                                 *
* 1. Redistributions of source code must retain the above copyright notice,       *
* this list of conditions and the following disclaimer.                           *
*                                                                                 *
* 2. Redistributions in binary form must reproduce the above copyright            *
* notice, this list of conditions and the following disclaimer in the             *
* documentation and/or other materials provided with the distribution.            *
*                                                                                 *
* 3. Neither the name of the copyright holder nor the names of its contributors   *
* may be used to endorse or promote products derived from this software without   *
* specific prior written permission.                                              *
*                                                                                 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"     *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE       *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  *
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    *
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      *
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      *
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   *
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            *
*                                                                                 *
**********************************************************************************/
/*
 * gf32_vmull.cpp
 *
 * Routines for 64-bit Galois fields using NEON.
 *
 */

#include <arm_neon.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "gf32_vmull.h"

typedef poly16x8_t __v16p;
typedef poly8x8_t __v8x8p;
typedef uint8x8_t __v8x8u;
typedef uint8x16_t __v8u;
typedef uint16x8_t __v16u;
typedef uint64x2_t __v64u;
typedef uint32x4_t __v32u;
typedef uint32x2_t __32u;

static inline uint16x8_t mul64 (uint32_t a32, uint32_t b32) {
    // we are going to make a into 64 bits to make shifting, circual
    // a = [a32,a32]
    uint64x1_t a = vreinterpret_u64_u32 (vdup_n_u32 ((uint32_t)a32));
    poly8x8_t b = vcreate_p8 (b32);
    poly16x8_t res0, res1, res2, res3, res4;
    uint64x2_t mask1 = vcombine_u64 (vcreate_u64 (0x000000000000ffff), vcreate_u64 (0));
    uint64x2_t mask2 = vcombine_u64 (vcreate_u64 (0x00000000ffffffff), vcreate_u64 (0));
    uint64x2_t mask3 = vcombine_u64 (vcreate_u64 (0xffff000000000000), vcreate_u64 (0));

    // take the first 32 bits, convert to polynomial and multiply a*b
    // as an array [0,1,2,3] of a by [0,1,2,3] of b we get [00,11,22,33]
    // res0 = [00,11,22,33]
    res0 = vmull_p8 (vcreate_p8 (vget_lane_u32 (vreinterpret_u32_u64 (a), 0)), b);

    // shift a by 8 bits, we now have [a32>>8,a32>>8], so from the point of view of an array
    // we have [1,2,3,0] for our 4 8 bit elements in the 32 bit value
    a = vshr_n_u64 (a, 8);
    // multiply a32 [1,2,3,0] by b32 [0,1,2,3] to get [01,12,23,30]
    // res1 = [01,12,23,30]
    res1 = vmull_p8 (vcreate_p8 (vget_lane_u32 (vreinterpret_u32_u64 (a), 0)), b);

    // we are going to shift a again by 8 so now a is [a32>>16,a32>>16] as an array for a32
    // a32 = [2,3,0,1]
    a = vshr_n_u64 (a, 8);
    // multiply a32 [2,3,0,1] by b32 [0,1,2,3] we get [02,13,20,31]
    // res2 = [02,13,20,13]
    res2 = vmull_p8 (vcreate_p8 (vget_lane_u32 (vreinterpret_u32_u64 (a), 0)), b);

    // the last set of multiplication of a and b as a32>>24 this time
    a = vshr_n_u64 (a, 8);
    // multiply a32 [3,0,1,2] by b32 [0,1,2,3] we get [03,10,21,32]
    res3 = vmull_p8 (vcreate_p8 (vget_lane_u32 (vreinterpret_u32_u64 (a), 0)), b);

    // at this point res0-res3 hold all of the multiplications we need.
    // we have multiplied each value by each other value

    // but now we must align the data correctly, i.e that when we shifted a32>>8
    // that we correctly shift the result <<8.

    // res0 requires 0 shifts, that is done


    // we will use res4 as a temporary register for shifting.
    // res1 requires 1 shift by 8, this will be broken into 3 steps
    // step 1: get the low 24 bits and shift them left by 8, the mask is because we are dealing
    // will 64 bits, but want to only operate on 32
    res4 = vreinterpretq_p16_u64 (
    vshlq_n_u64 (vandq_u64 (vreinterpretq_u64_p16 (res1),
                            vreinterpretq_u64_u16 (vmvnq_u16 (vreinterpretq_u16_u64 (mask3)))),
                 8));
    // step 2: get the high 8 bits, and shift those by 24 to the right, this is overflow in field
    res1 =
    vreinterpretq_p16_u64 (vshrq_n_u64 (vandq_u64 (vreinterpretq_u64_p16 (res1), mask3), 24));
    // step 3: xor together vectors from step 1 and step 2 to get our shifted res1
    res1 =
    vreinterpretq_p16_u64 (veorq_u64 (vreinterpretq_u64_p16 (res1), vreinterpretq_u64_p16 (res4)));

    // same plan as the last round, but here we shifted by 16.  The logic is simplier because we
    // will
    // shift the low 16 to the left by 16, and the high 16 to the right by 16.
    res4 =
    vreinterpretq_p16_u64 (vshlq_n_u64 (vandq_u64 (mask2, vreinterpretq_u64_p16 (res2)), 16));
    res2 = vreinterpretq_p16_u64 (
    vshrq_n_u64 (vandq_u64 (vreinterpretq_u64_u16 (vmvnq_u16 (vreinterpretq_u16_u64 (mask2))),
                            vreinterpretq_u64_p16 (res2)),
                 16));
    res2 =
    vreinterpretq_p16_u64 (veorq_u64 (vreinterpretq_u64_p16 (res2), vreinterpretq_u64_p16 (res4)));

    // in the last multiplication it is an inverse of the first step, we will take the low
    // 8 bits and shift left by 24, take the high 24 bits and shift right by 8
    res4 = vreinterpretq_p16_u64 (
    vshrq_n_u64 (vandq_u64 (vreinterpretq_u64_p16 (res3),
                            vreinterpretq_u64_u16 (vmvnq_u16 (vreinterpretq_u16_u64 (mask1)))),
                 8));
    res3 =
    vreinterpretq_p16_u64 (vshlq_n_u64 (vandq_u64 (vreinterpretq_u64_p16 (res3), mask1), 24));
    res3 =
    vreinterpretq_p16_u64 (veorq_u64 (vreinterpretq_u64_p16 (res3), vreinterpretq_u64_p16 (res4)));

    // here we consolidate all the results together
    // res0 = res0 + res1 + res3
    res0 = vreinterpretq_p16_u64 (
    veorq_u64 (vreinterpretq_u64_p16 (res0),
               veorq_u64 (vreinterpretq_u64_p16 (res1), vreinterpretq_u64_p16 (res3))));
    // res0 = res0 + res2
    res0 =
    vreinterpretq_p16_u64 (veorq_u64 (vreinterpretq_u64_p16 (res0), vreinterpretq_u64_p16 (res2)));

    return vreinterpretq_u16_p16 (res0);
}

gf_32_t gf32_vmull_state::mul (gf_32_t a32, gf_32_t b32) const {
    uint64x1_t mk = vcreate_u64 (0x00000000ffff0000);
    uint64x1_t v;
    uint16x8_t w, result;

    result = mul64 (a32, b32);
    v = vshr_n_u64 (vget_low_u64 (vreinterpretq_u64_u16 (result)), 32);
    v = vand_u64 (v, mk);
    w = mul64 (prim_poly & 0xffff, vget_lane_u32 (vreinterpret_u32_u64 (v), 0));

    result = veorq_u16 (result, w);
    v = vshr_n_u64 (vget_low_u64 (vreinterpretq_u64_u16 (result)), 32);
    v = vand_u64 (v, vshr_n_u64 (mk, 16));
    w = mul64 (prim_poly & 0xffff, vget_lane_u32 (vreinterpret_u32_u64 (v), 0));

    result = veorq_u16 (result, w);
    return (vgetq_lane_u32 (vreinterpretq_u32_u16 (result), 0));
}

unsigned gf32_vmull_state::mult_table_size () const { return (0); }

void gf32_vmull_state::mul_region (const gf_32_t *src,
                                   gf_32_t *dst,
                                   uint64_t bytes,
                                   gf_32_t m,
                                   bool accum,
                                   void *mult_table,
                                   bool mult_table_valid) const {
    if (m == (gf_32_t)0) {
        if (!accum) {
            memset (dst, 0, bytes);
        }
        return;
    } else if (m == (gf_32_t)1) {
        if (!accum) {
            memcpy (dst, src, bytes);
        } else {
            uint64_t chunks = bytes >> 3ULL;
            for (uint64_t i = 0; i < chunks; i += 2) {
                __v64u x = vld1q_u64 ((uint64_t *)src + i);
                __v64u y = vld1q_u64 ((uint64_t *)dst + i);
                vst1q_u64 ((uint64_t *)dst + i, veorq_u64 (x, y));
            }
        }
        return;
    }

    // ignore compiler unused warning
    (void)mult_table;
    (void)mult_table_valid;

    uint64_t i;
    uint64_t chunks = bytes >> 2ULL;
    if (accum) {
        for (i = 0; i < chunks; i++) {
            dst[i] ^= mul (src[i], m);
        }
    } else {
        for (i = 0; i < chunks; i++) {
            dst[i] = mul (src[i], m);
        }
    }
}

void gf32_vmull_state::mul_region_region (const gf_32_t *src1,
                                          const gf_32_t *src2,
                                          gf_32_t *dst,
                                          uint64_t bytes,
                                          bool accum) const {

    uint64_t i;
    uint64_t chunks = bytes >> 2ULL;
    if (accum) {
        for (i = 0; i < chunks; i++) {
            dst[i] ^= mul (src1[i], src2[i]);
        }
    } else {
        for (i = 0; i < chunks; i++) {
            dst[i] = mul (src1[i], src2[i]);
        }
    }
}

gf_32_t gf32_vmull_state::dotproduct (const gf_32_t *src1,
                                      const gf_32_t *src2,
                                      uint64_t n_elements,
                                      uint64_t stride) const {
    gf_32_t dp = (gf_32_t)0;
    for (uint64_t i = 0; i < n_elements; ++i) {
        dp = add (dp, mul (*src1, *src2));
        src1 += stride;
        src2 += stride;
    }
    return dp;
}
