
/**********************************************************************************
*                                                                                 *
*     Gferasure was developed by the Center for Research in Storage Systems       *
*                         http://www.crss.ucsc.edu/                               *
*                                                                                 *
* BSD 3-Clause License                                                            *
*                                                                                 *
* Copyright (c) 2016, Regents of the University of California                     *
*                                                                                 *
* All rights reserved.                                                            *
*                                                                                 *
* Redistribution and use in source and binary forms, with or without              *
* modification, are permitted provided that the following conditions are met:     *
*                                                                                 *
* 1. Redistributions of source code must retain the above copyright notice,       *
* this list of conditions and the following disclaimer.                           *
*                                                                                 *
* 2. Redistributions in binary form must reproduce the above copyright            *
* notice, this list of conditions and the following disclaimer in the             *
* documentation and/or other materials provided with the distribution.            *
*                                                                                 *
* 3. Neither the name of the copyright holder nor the names of its contributors   *
* may be used to endorse or promote products derived from this software without   *
* specific prior written permission.                                              *
*                                                                                 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"     *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE       *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  *
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    *
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      *
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      *
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   *
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            *
*                                                                                 *
**********************************************************************************/
/*
 * gf32_clmul.cpp
 *
 * Routines for 32-bit Galois fields using SSSE3.
 *
 */

#include "gf32_clmul.h"

#include <stdio.h>
#include <string.h>
#include <x86intrin.h>


static inline __m128i mul_4 (__m128i a, __m128i b, __m128i pp) {

    __m128i res0, res1;
    __m128i v0, w0;
    __m128i mask = _mm_set_epi32 (0, 0, 0, 0xffffffff);

    res0 = _mm_clmulepi64_si128 (_mm_and_si128 (a, mask), b, 0);
    v0 = _mm_srli_epi64 (res0, 32);
    w0 = _mm_clmulepi64_si128 (pp, v0, 0);
    res0 = _mm_xor_si128 (res0, w0);
    v0 = _mm_srli_epi64 (res0, 32);
    w0 = _mm_clmulepi64_si128 (pp, v0, 0);
    res1 = _mm_xor_si128 (res0, w0);

    // begin loop here
    res0 = _mm_clmulepi64_si128 (_mm_and_si128 (_mm_srli_si128 (a, 4), mask), b, 0);
    v0 = _mm_srli_epi64 (res0, 32);
    w0 = _mm_clmulepi64_si128 (pp, v0, 0);
    res0 = _mm_xor_si128 (res0, w0);
    v0 = _mm_srli_epi64 (res0, 32);
    w0 = _mm_clmulepi64_si128 (pp, v0, 0);
    res1 = _mm_xor_si128 (res1, _mm_slli_si128 (_mm_xor_si128 (res0, w0), 4));

    res0 = _mm_clmulepi64_si128 (_mm_and_si128 (_mm_srli_si128 (a, 8), mask), b, 0);
    v0 = _mm_srli_epi64 (res0, 32);
    w0 = _mm_clmulepi64_si128 (pp, v0, 0);
    res0 = _mm_xor_si128 (res0, w0);
    v0 = _mm_srli_epi64 (res0, 32);
    w0 = _mm_clmulepi64_si128 (pp, v0, 0);
    res1 = _mm_xor_si128 (res1, _mm_slli_si128 (_mm_xor_si128 (res0, w0), 8));

    res0 = _mm_clmulepi64_si128 (_mm_and_si128 (_mm_srli_si128 (a, 12), mask), b, 0);
    v0 = _mm_srli_epi64 (res0, 32);
    w0 = _mm_clmulepi64_si128 (pp, v0, 0);
    res0 = _mm_xor_si128 (res0, w0);
    v0 = _mm_srli_epi64 (res0, 32);
    w0 = _mm_clmulepi64_si128 (pp, v0, 0);
    res1 = _mm_xor_si128 (res1, _mm_slli_si128 (_mm_xor_si128 (res0, w0), 12));

    return res1;
}

static inline __m128i mul_4_region (__m128i a, __m128i b, __m128i pp) {

    __m128i res0, res1;
    __m128i v0, w0;
    __m128i mask = _mm_set_epi32 (0, 0, 0, 0xffffffff);

    res0 = _mm_clmulepi64_si128 (_mm_and_si128 (a, mask), _mm_and_si128 (b, mask), 0);
    v0 = _mm_srli_epi64 (res0, 32);
    w0 = _mm_clmulepi64_si128 (pp, v0, 0);
    res0 = _mm_xor_si128 (res0, w0);
    v0 = _mm_srli_epi64 (res0, 32);
    w0 = _mm_clmulepi64_si128 (pp, v0, 0);
    res1 = _mm_xor_si128 (res0, w0);

    // begin loop here
    res0 = _mm_clmulepi64_si128 (_mm_and_si128 (_mm_srli_si128 (a, 4), mask),
                                 _mm_and_si128 (_mm_srli_si128 (b, 4), mask), 0);
    v0 = _mm_srli_epi64 (res0, 32);
    w0 = _mm_clmulepi64_si128 (pp, v0, 0);
    res0 = _mm_xor_si128 (res0, w0);
    v0 = _mm_srli_epi64 (res0, 32);
    w0 = _mm_clmulepi64_si128 (pp, v0, 0);
    res1 = _mm_xor_si128 (res1, _mm_slli_si128 (_mm_xor_si128 (res0, w0), 4));

    res0 = _mm_clmulepi64_si128 (_mm_and_si128 (_mm_srli_si128 (a, 8), mask),
                                 _mm_and_si128 (_mm_srli_si128 (b, 8), mask), 0);
    v0 = _mm_srli_epi64 (res0, 32);
    w0 = _mm_clmulepi64_si128 (pp, v0, 0);
    res0 = _mm_xor_si128 (res0, w0);
    v0 = _mm_srli_epi64 (res0, 32);
    w0 = _mm_clmulepi64_si128 (pp, v0, 0);
    res1 = _mm_xor_si128 (res1, _mm_slli_si128 (_mm_xor_si128 (res0, w0), 8));

    res0 = _mm_clmulepi64_si128 (_mm_and_si128 (_mm_srli_si128 (a, 12), mask),
                                 _mm_and_si128 (_mm_srli_si128 (b, 12), mask), 0);
    v0 = _mm_srli_epi64 (res0, 32);
    w0 = _mm_clmulepi64_si128 (pp, v0, 0);
    res0 = _mm_xor_si128 (res0, w0);
    v0 = _mm_srli_epi64 (res0, 32);
    w0 = _mm_clmulepi64_si128 (pp, v0, 0);
    res1 = _mm_xor_si128 (res1, _mm_slli_si128 (_mm_xor_si128 (res0, w0), 12));

    return res1;
}

gf_32_t gf32_clmul_state::mul (gf_32_t a32, gf_32_t b32) const {
    __m128i a, b;
    __m128i result;
    __m128i pp;
    __m128i v, w;
    a = _mm_insert_epi32 (_mm_setzero_si128 (), a32, 0);
    b = _mm_insert_epi32 (a, b32, 0);
    pp = _mm_set_epi32 (0, 0, 1, (uint32_t) (prim_poly & 0xffffULL));
    result = _mm_clmulepi64_si128 (a, b, 0);
    v = _mm_srli_epi64 (result, 32);
    w = _mm_clmulepi64_si128 (pp, v, 0);
    result = _mm_xor_si128 (result, w);
    v = _mm_srli_epi64 (result, 32);
    w = _mm_clmulepi64_si128 (pp, v, 0);
    result = _mm_xor_si128 (result, w);
    return _mm_extract_epi32 (result, 0);
}

unsigned gf32_clmul_state::mult_table_size () const { return (0); }

void gf32_clmul_state::mul_region (const gf_32_t *src,
                                   gf_32_t *dst,
                                   uint64_t bytes,
                                   gf_32_t m,
                                   bool accum,
                                   void *mult_table,
                                   bool mult_table_valid) const {
    if (m == (gf_32_t)0) {
        if (!accum) {
            memset (dst, 0, bytes);
        }
        return;
    } else if (m == (gf_64_t)1) {
        if (!accum) {
            memcpy (dst, src, bytes);
        } else {
            uint64_t chunks = bytes >> 4ULL;
            for (uint64_t i = 0; i < chunks; i++) {
                __m128i x = _mm_loadu_si128 (((__m128i *)src) + i);
                __m128i y = _mm_loadu_si128 (((__m128i *)dst) + i);
                _mm_storeu_si128 (((__m128i *)dst) + i, _mm_xor_si128 (x, y));
            }
        }
        return;
    }

    // ignore compiler unused warning
    (void)mult_table;
    (void)mult_table_valid;

    uint32_t i;
    uint32_t chunks = bytes >> 4ULL;
    __m128i x, y;
    __m128i mm = _mm_set_epi32 (0, 0, 0, m);
    __m128i pp = _mm_set_epi32 (0, 0, 1, (uint32_t) (prim_poly & 0xffffULL));

    if (accum) {
        for (i = 0; i < chunks; i++) {
            x = _mm_loadu_si128 (((__m128i *)src) + i);
            y = _mm_loadu_si128 (((__m128i *)dst) + i);
            x = mul_4 (x, mm, pp);
            _mm_storeu_si128 (((__m128i *)dst) + i, _mm_xor_si128 (x, y));
        }
    } else {
        for (i = 0; i < chunks; i++) {
            x = _mm_loadu_si128 (((__m128i *)src) + i);
            x = mul_4 (x, mm, pp);
            _mm_storeu_si128 (((__m128i *)dst) + i, x);
        }
    }
}

void gf32_clmul_state::mul_region_region (const gf_32_t *src1,
                                          const gf_32_t *src2,
                                          gf_32_t *dst,
                                          uint64_t bytes,
                                          bool accum) const {

    uint32_t i;
    uint32_t chunks = bytes >> 4ULL;
    __m128i pp = _mm_set_epi32 (0, 0, 1, (uint32_t) (prim_poly & 0xffffULL));

    if (accum) {
        __m128i x, y, z;
        for (i = 0; i < chunks; i++) {
            x = _mm_loadu_si128 (((__m128i *)src1) + i);
            y = _mm_loadu_si128 (((__m128i *)src2) + i);
            z = _mm_loadu_si128 (((__m128i *)dst) + i);
            _mm_storeu_si128 (((__m128i *)dst) + i, _mm_xor_si128 (mul_4_region (x, y, pp), z));
        }
    } else {
        __m128i x, y;
        for (i = 0; i < chunks; i++) {
            x = _mm_loadu_si128 (((__m128i *)src1) + i);
            y = _mm_loadu_si128 (((__m128i *)src2) + i);
            _mm_storeu_si128 (((__m128i *)dst) + i, mul_4_region (x, y, pp));
        }
    }
}

gf_32_t gf32_clmul_state::dotproduct (const gf_32_t *src1,
                                      const gf_32_t *src2,
                                      uint64_t n_elements,
                                      uint64_t stride) const {

    gf_32_t dp = (gf_32_t)0;

    for (uint32_t i = 0; i < n_elements; ++i) {
        dp = add (dp, mul (*src1, *src2));
        src1 += stride;
        src2 += stride;
    }
    return dp;
}
