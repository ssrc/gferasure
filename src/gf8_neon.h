
/**********************************************************************************
*                                                                                 *
*     Gferasure was developed by the Center for Research in Storage Systems       *
*                         http://www.crss.ucsc.edu/                               *
*                                                                                 *
* BSD 3-Clause License                                                            *
*                                                                                 *
* Copyright (c) 2016, Regents of the University of California                     *
*                                                                                 *
* All rights reserved.                                                            *
*                                                                                 *
* Redistribution and use in source and binary forms, with or without              *
* modification, are permitted provided that the following conditions are met:     *
*                                                                                 *
* 1. Redistributions of source code must retain the above copyright notice,       *
* this list of conditions and the following disclaimer.                           *
*                                                                                 *
* 2. Redistributions in binary form must reproduce the above copyright            *
* notice, this list of conditions and the following disclaimer in the             *
* documentation and/or other materials provided with the distribution.            *
*                                                                                 *
* 3. Neither the name of the copyright holder nor the names of its contributors   *
* may be used to endorse or promote products derived from this software without   *
* specific prior written permission.                                              *
*                                                                                 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"     *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE       *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  *
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    *
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      *
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      *
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   *
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            *
*                                                                                 *
**********************************************************************************/
//
// gf8_neon.h
//
// 8-bit GF using NEON extensions.

#pragma once

#include "gf_random.h"
#include "gf_types.h"
#include "gfw_log.h"

struct alignas (16) gf8_neon_state {

    gf8_neon_state (gf_8_t prim_poly_ = gf8_default_primpoly, gf_8_t alpha_ = 1)
    : prim_poly (0x100 | prim_poly_), log_state (prim_poly_, alpha_) {}

    // Unary operators
    gf_8_t inv (gf_8_t a) const { return log_state.inv (a); }
    gf_8_t log (gf_8_t a) const { return log_state.log (a); }
    gf_8_t antilog (gf_8_t a) const { return log_state.antilog (a); }
    // Binary operators
    gf_8_t add (gf_8_t a, gf_8_t b) const { return ((gf_8_t) (a ^ b)); }
    gf_8_t sub (gf_8_t a, gf_8_t b) const { return ((gf_8_t) (a ^ b)); }
    gf_8_t mul (gf_8_t a, gf_8_t b) const { return log_state.mul (a, b); }
    gf_8_t div (gf_8_t a, gf_8_t b) const { return log_state.div (a, b); }
    gf_8_t pow (gf_8_t a, uint32_t b) const { return log_state.pow (a, b); }

    // Region operators
    void mul_region (const gf_8_t *src,
                     gf_8_t *dst,
                     uint64_t bytes,
                     gf_8_t m,
                     bool accum = false,
                     void *mult_table = NULL,
                     bool mult_table_valid = false) const;
    void mul_region_region (const gf_8_t *src1,
                            const gf_8_t *src2,
                            gf_8_t *dst,
                            uint64_t bytes,
                            bool accum = false) const;
    gf_8_t dotproduct (const gf_8_t *src1,
                       const gf_8_t *src2,
                       uint64_t n_elements,
                       uint64_t stride = 1) const;

    bool rs_encode (gf_8_t **const data,
                    unsigned n_data,
                    gf_8_t **parity,
                    unsigned n_parity,
                    uint64_t length) const;
    uint64_t field_size () const { return GF8_FIELD_SIZE; }
    uint64_t field_max () const { return (field_size () - 1); }
    unsigned mult_table_size () const;

    private:
    uint32_t prim_poly;
    gf8_log_state log_state;
    gf_8_t reduce_tbl[16];
};
