import os
import subprocess
import csv
import datetime
import numpy as np
import scipy.stats
import pprint
import time
import socket

range_0 = range(2**10, 2**19,2**10) # 1k - 256k, by 1k
#range_1 = range((2**18),2**21,(2**18)) # 256k - 2mb, by 256k

#fields = ["gf8_neon","gf16_neon","gf8_vmull","gf8_log","gf16_log","gf8_shiftadd", "gf16_shiftadd"]
fields = ["gf32_vmull","gf64_vmull"]
oper = "Tm"
trange = range_0#+range_1
runs = 50 ## average over X runs

for field in fields:
  file_n = "_LOG"+socket.gethostname()+"__"+oper+"_"+field+".txt"
  res_fi = open(file_n,'w')
 
  for i in trange:
    size = 1024
    gf8a = []
    for k in range(0,runs):
      p = subprocess.Popen(["./gf_test", "-%s" % oper, "-b", "%s" % i, "-t","256", "-g","%s" % field],\
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      out, err = p.communicate()
      for line in str(out).split("\n"):
        line = str(line[2:-3])
        if line:
          name = line.split(":")[0]
          ## take bytes * trials / KB
          tsize = float(line.split(" ")[-4])/(1024.0)
          if size != tsize:
            size = tsize
          if name == field:
            tres = float(line.split("(")[1][:-1].split(" ")[0])
            gf8a.append(tres)

    gf8a_avg   = float(np.mean(gf8a))
    gf8a_dev   = float(np.std(gf8a))
    gf8a_err   = float(scipy.stats.sem(gf8a))
    li = str(size) + "," + str(gf8a_avg) + "," + str(gf8a_dev) + "," + str(gf8a_err)+"\n"
    res_fi.write(li)
    res_fi.flush()
    print(li.strip())
  res_fi.close()
