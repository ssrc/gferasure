
/**********************************************************************************
*                                                                                 *
*     Gferasure was developed by the Center for Research in Storage Systems       *
*                         http://www.crss.ucsc.edu/                               *
*                                                                                 *
* BSD 3-Clause License                                                            *
*                                                                                 *
* Copyright (c) 2016, Regents of the University of California                     *
*                                                                                 *
* All rights reserved.                                                            *
*                                                                                 *
* Redistribution and use in source and binary forms, with or without              *
* modification, are permitted provided that the following conditions are met:     *
*                                                                                 *
* 1. Redistributions of source code must retain the above copyright notice,       *
* this list of conditions and the following disclaimer.                           *
*                                                                                 *
* 2. Redistributions in binary form must reproduce the above copyright            *
* notice, this list of conditions and the following disclaimer in the             *
* documentation and/or other materials provided with the distribution.            *
*                                                                                 *
* 3. Neither the name of the copyright holder nor the names of its contributors   *
* may be used to endorse or promote products derived from this software without   *
* specific prior written permission.                                              *
*                                                                                 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"     *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE       *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  *
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    *
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      *
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      *
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   *
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            *
*                                                                                 *
**********************************************************************************/
// Fields instantiated for gf_matrix.
//
// For each field, this includes both the gf_matrix instance and the default field.
//

// Always instantiate shiftadd fields
template struct gf_matrix< gf_8_t, gf8_shiftadd_state >;
template struct gf_matrix< gf_16_t, gf16_shiftadd_state >;
template struct gf_matrix< gf_32_t, gf32_shiftadd_state >;
template struct gf_matrix< gf_64_t, gf64_shiftadd_state >;
template <>
gf_w_state< gf_8_t, gf8_shiftadd_state >
gf_matrix< gf_8_t, gf8_shiftadd_state >::default_field ((gf_8_t)0x1d, 1);
template <>
gf_w_state< gf_16_t, gf16_shiftadd_state >
gf_matrix< gf_16_t, gf16_shiftadd_state >::default_field ((gf_16_t)0x100b, 1);
template <>
gf_w_state< gf_32_t, gf32_shiftadd_state >
gf_matrix< gf_32_t, gf32_shiftadd_state >::default_field ((gf_32_t)0x000000c5, 1);
template <>
gf_w_state< gf_64_t, gf64_shiftadd_state >
gf_matrix< gf_64_t, gf64_shiftadd_state >::default_field ((gf_64_t)0x000000000000001b, 1);

#ifdef GF_LOG
template <>
gf_w_state< gf_8_t, gf8_log_state > gf_matrix< gf_8_t, gf8_log_state >::default_field ((gf_8_t)0x1d,
                                                                                       (gf_8_t)1);
template <>
gf_w_state< gf_16_t, gf16_log_state >
gf_matrix< gf_16_t, gf16_log_state >::default_field ((gf_16_t)0x100b, (gf_16_t)1);
template struct gf_matrix< gf_8_t, gf8_log_state >;
template struct gf_matrix< gf_16_t, gf16_log_state >;
#endif // GF_LOG

#ifdef GF_SSSE3
template struct gf_matrix< gf_8_t, gf8_ssse3_state >;
template struct gf_matrix< gf_16_t, gf16_ssse3_state >;
template struct gf_matrix< gf_32_t, gf32_ssse3_state >;
template <>
gf_w_state< gf_8_t, gf8_ssse3_state >
gf_matrix< gf_8_t, gf8_ssse3_state >::default_field ((gf_8_t)0x1d, (gf_8_t)1);
template <>
gf_w_state< gf_16_t, gf16_ssse3_state >
gf_matrix< gf_16_t, gf16_ssse3_state >::default_field ((gf_16_t)0x100b, (gf_16_t)1);
template <>
gf_w_state< gf_32_t, gf32_ssse3_state >
gf_matrix< gf_32_t, gf32_ssse3_state >::default_field ((gf_32_t)0x000000c5, (gf_32_t)1);
#endif // GF_SSSE3

#ifdef GF_AVX
template struct gf_matrix< gf_32_t, gf32_avx_shift2_state >;
template <>
gf_w_state< gf_32_t, gf32_avx_shift2_state >
gf_matrix< gf_32_t, gf32_avx_shift2_state >::default_field ((gf_32_t)0x000000c5, 1);
#endif // GF_AVX

#ifdef GF_AVX2
template struct gf_matrix< gf_8_t, gf8_avx2_state >;
template struct gf_matrix< gf_16_t, gf16_avx2_state >;
template struct gf_matrix< gf_8_t, gf8_gather_state >;
template struct gf_matrix< gf_16_t, gf16_gather_state >;
template <>
gf_w_state< gf_8_t, gf8_avx2_state >
gf_matrix< gf_8_t, gf8_avx2_state >::default_field ((gf_8_t)0x1d, (gf_8_t)1);
template <>
gf_w_state< gf_16_t, gf16_avx2_state >
gf_matrix< gf_16_t, gf16_avx2_state >::default_field ((gf_16_t)0x100b, (gf_16_t)1);
template <>
gf_w_state< gf_8_t, gf8_gather_state >
gf_matrix< gf_8_t, gf8_gather_state >::default_field ((gf_8_t)0x1d, (gf_8_t)1);
template <>
gf_w_state< gf_16_t, gf16_gather_state >
gf_matrix< gf_16_t, gf16_gather_state >::default_field ((gf_16_t)0x100b, (gf_16_t)1);
#endif // GF_AVX2

#ifdef GF_CLMUL
template struct gf_matrix< gf_8_t, gf8_clmul_state >;
template struct gf_matrix< gf_16_t, gf16_clmul_state >;
template struct gf_matrix< gf_32_t, gf32_clmul_state >;
template struct gf_matrix< gf_64_t, gf64_clmul_state >;
template <>
gf_w_state< gf_8_t, gf8_clmul_state >
gf_matrix< gf_8_t, gf8_clmul_state >::default_field ((gf_8_t)0x1d, 1);
template <>
gf_w_state< gf_16_t, gf16_clmul_state >
gf_matrix< gf_16_t, gf16_clmul_state >::default_field ((gf_16_t)0x002d, 1);
template <>
gf_w_state< gf_32_t, gf32_clmul_state >
gf_matrix< gf_32_t, gf32_clmul_state >::default_field ((gf_32_t)0x000000c5, 1);
template <>
gf_w_state< gf_64_t, gf64_clmul_state >
gf_matrix< gf_64_t, gf64_clmul_state >::default_field ((gf_64_t)0x000000000000001b, 1);
#endif // GF_CLMUL

#if defined(GF_ARMV7) || defined(GF_ARMV8)
template struct gf_matrix< gf_8_t, gf8_neon_state >;
template struct gf_matrix< gf_16_t, gf16_neon_state >;
template struct gf_matrix< gf_8_t, gf8_vmull_state >;
template struct gf_matrix< gf_16_t, gf16_vmull_state >;
template struct gf_matrix< gf_32_t, gf32_vmull_state >;
template struct gf_matrix< gf_64_t, gf64_vmull_state >;
template <>
gf_w_state< gf_8_t, gf8_neon_state >
gf_matrix< gf_8_t, gf8_neon_state >::default_field ((gf_8_t)0x1d, (gf_8_t)1);
template <>
gf_w_state< gf_16_t, gf16_neon_state >
gf_matrix< gf_16_t, gf16_neon_state >::default_field ((gf_16_t)0x100b, (gf_16_t)1);
template <>
gf_w_state< gf_8_t, gf8_vmull_state >
gf_matrix< gf_8_t, gf8_vmull_state >::default_field ((gf_8_t)0x1d, 1);
template <>
gf_w_state< gf_16_t, gf16_vmull_state >
gf_matrix< gf_16_t, gf16_vmull_state >::default_field ((gf_16_t)0x002d, 1);
template <>
gf_w_state< gf_32_t, gf32_vmull_state >
gf_matrix< gf_32_t, gf32_vmull_state >::default_field ((gf_32_t)0x000000c5, 1);
template <>
gf_w_state< gf_64_t, gf64_vmull_state >
gf_matrix< gf_64_t, gf64_vmull_state >::default_field ((gf_64_t)0x000000000000001b, 1);
#endif // GF_NEON

#ifdef GF_PMULL
template struct gf_matrix< gf_8_t, gf8_pmull_state >;
template struct gf_matrix< gf_16_t, gf16_pmull_state >;
template struct gf_matrix< gf_32_t, gf32_pmull_state >;
template struct gf_matrix< gf_64_t, gf64_pmull_state >;
template <>
gf_w_state< gf_8_t, gf8_pmull_state >
gf_matrix< gf_8_t, gf8_pmull_state >::default_field ((gf_8_t)0x1d, 1);
template <>
gf_w_state< gf_16_t, gf16_pmull_state >
gf_matrix< gf_16_t, gf16_pmull_state >::default_field ((gf_16_t)0x002d, 1);
template <>
gf_w_state< gf_32_t, gf32_pmull_state >
gf_matrix< gf_32_t, gf32_pmull_state >::default_field ((gf_32_t)0x000000c5, 1);
template <>
gf_w_state< gf_64_t, gf64_pmull_state >
gf_matrix< gf_64_t, gf64_pmull_state >::default_field ((gf_64_t)0x000000000000001b, 1);
#endif // GF_PMULL
