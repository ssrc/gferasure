
/**********************************************************************************
*                                                                                 *
*     Gferasure was developed by the Center for Research in Storage Systems       *
*                         http://www.crss.ucsc.edu/                               *
*                                                                                 *
* BSD 3-Clause License                                                            *
*                                                                                 *
* Copyright (c) 2016, Regents of the University of California                     *
*                                                                                 *
* All rights reserved.                                                            *
*                                                                                 *
* Redistribution and use in source and binary forms, with or without              *
* modification, are permitted provided that the following conditions are met:     *
*                                                                                 *
* 1. Redistributions of source code must retain the above copyright notice,       *
* this list of conditions and the following disclaimer.                           *
*                                                                                 *
* 2. Redistributions in binary form must reproduce the above copyright            *
* notice, this list of conditions and the following disclaimer in the             *
* documentation and/or other materials provided with the distribution.            *
*                                                                                 *
* 3. Neither the name of the copyright holder nor the names of its contributors   *
* may be used to endorse or promote products derived from this software without   *
* specific prior written permission.                                              *
*                                                                                 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"     *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE       *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  *
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    *
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      *
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      *
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   *
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            *
*                                                                                 *
**********************************************************************************/
//
//      Erasure code routines
//

#pragma once
#include "gf_w.h"

//
// Note that the underlying field is a per-GF width static structure.  This is defined in
// gf_erasure.cpp, and is initialized at load time.  It may be necessary to ensure that
// initializing the field doesn't allocate memory.
//

template < class gf_w, class gf_state > struct gf_matrix {

    static gf_w_state< gf_w, gf_state > default_field;

    gf_matrix (unsigned rows_,
               unsigned columns_,
               const gf_w_state< gf_w, gf_state > &field_ = default_field);
    gf_matrix (const gf_matrix &m);
    ~gf_matrix ();

    int invert (gf_matrix< gf_w, gf_state > *result);
    void multiply_regions (const gf_w *const *in, gf_w *out[]);
    bool set_vandermonde (const gf_w *row_basis = (gf_w *)0);
    bool
    set_cauchy (bool systematic, const gf_w *row_ids = (gf_w *)0, const gf_w *col_ids = (gf_w *)0);
    void set_cauchy ();
    void set_zero ();
    void set_identity ();
    void copy_column (unsigned dst_column,
                      const gf_matrix< gf_w, gf_state > &src_matrix,
                      unsigned src_column);
    void
    copy_row (unsigned dst_row, const gf_matrix< gf_w, gf_state > &src_matrix, unsigned src_row);
    void exchange_rows (unsigned row_a, unsigned row_b);
    void apply (const gf_w *in, gf_w *out) const;
    void apply (const gf_w *const *in,
                gf_w *out[],
                uint64_t sz,
                uint64_t stripe_size = default_stripe_size) const;
    void apply (const gf_w *const *in,
                gf_w *out[],
                uint64_t sz,
                bool cache,
                uint64_t stripe_size = default_stripe_size) const;
    gf_w &e (unsigned r, unsigned c) const {
        // Values are stored row-major.
        return values[r * n_cols + c];
    }
    gf_w &operator() (unsigned r, unsigned c) const { return e (r, c); }

    void print (const char *name = NULL) const;

    unsigned n_rows;
    unsigned n_cols;
    gf_w *values;
    const gf_w_state< gf_w, gf_state > &field;
    static const uint64_t default_stripe_size = 4096;
};
