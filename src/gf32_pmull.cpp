
/**********************************************************************************
*                                                                                 *
*     Gferasure was developed by the Center for Research in Storage Systems       *
*                         http://www.crss.ucsc.edu/                               *
*                                                                                 *
* BSD 3-Clause License                                                            *
*                                                                                 *
* Copyright (c) 2016, Regents of the University of California                     *
*                                                                                 *
* All rights reserved.                                                            *
*                                                                                 *
* Redistribution and use in source and binary forms, with or without              *
* modification, are permitted provided that the following conditions are met:     *
*                                                                                 *
* 1. Redistributions of source code must retain the above copyright notice,       *
* this list of conditions and the following disclaimer.                           *
*                                                                                 *
* 2. Redistributions in binary form must reproduce the above copyright            *
* notice, this list of conditions and the following disclaimer in the             *
* documentation and/or other materials provided with the distribution.            *
*                                                                                 *
* 3. Neither the name of the copyright holder nor the names of its contributors   *
* may be used to endorse or promote products derived from this software without   *
* specific prior written permission.                                              *
*                                                                                 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"     *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE       *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  *
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    *
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      *
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      *
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   *
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            *
*                                                                                 *
**********************************************************************************/
/*
 * gf32_pmull.cpp
 *
 * Routines for 64-bit Galois fields using NEON.
 *
 */

#include <arm_neon.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "gf32_pmull.h"

typedef poly16x8_t __v16p;
typedef poly8x8_t __v8x8p;
typedef uint8x8_t __v8x8u;
typedef uint8x16_t __v8u;
typedef uint16x8_t __v16u;
typedef uint64x2_t __v64u;
typedef uint32x4_t __v32u;
typedef uint32x2_t __32u;

static inline uint16x8_t mul64 (uint64x1_t a, uint64x1_t b) {
    return vreinterpretq_u16_u64 ((uint64x2_t)vmull_p64 ((poly64_t)a, (poly64_t)b));
}

gf_32_t gf32_pmull_state::mul (gf_32_t a32, gf_32_t b32) const {
    uint64x1_t mk = vcreate_u64 (0x00000000ffff0000);
    uint64x1_t pp = vcreate_u64 (prim_poly & 0xffff);
    uint64x1_t v;
    uint16x8_t w, result;

    result = mul64 (vcreate_u64 (a32), vcreate_u64 (b32));
    v = vshr_n_u64 (vget_low_u64 (vreinterpretq_u64_u16 (result)), 32);
    v = vand_u64 (v, mk);
    w = mul64 (pp, v);

    result = veorq_u16 (result, w);
    v = vshr_n_u64 (vget_low_u64 (vreinterpretq_u64_u16 (result)), 32);
    v = vand_u64 (v, vshr_n_u64 (mk, 16));
    w = mul64 (pp, v);

    result = veorq_u16 (result, w);
    return (vgetq_lane_u32 (vreinterpretq_u32_u16 (result), 0));
}

unsigned gf32_pmull_state::mult_table_size () const { return (0); }

void gf32_pmull_state::mul_region (const gf_32_t *src,
                                   gf_32_t *dst,
                                   uint64_t bytes,
                                   gf_32_t m,
                                   bool accum,
                                   void *mult_table,
                                   bool mult_table_valid) const {
    if (m == (gf_32_t)0) {
        if (!accum) {
            memset (dst, 0, bytes);
        }
        return;
    } else if (m == (gf_32_t)1) {
        if (!accum) {
            memcpy (dst, src, bytes);
        } else {
            uint64_t chunks = bytes >> 3ULL;
            for (uint64_t i = 0; i < chunks; i += 2) {
                __v64u x = vld1q_u64 ((uint64_t *)src + i);
                __v64u y = vld1q_u64 ((uint64_t *)dst + i);
                vst1q_u64 ((uint64_t *)dst + i, veorq_u64 (x, y));
            }
        }
        return;
    }

    // ignore compiler unused warning
    (void)mult_table;
    (void)mult_table_valid;

    uint64_t i;
    uint64_t chunks = bytes >> 2ULL;
    if (accum) {
        for (i = 0; i < chunks; i++) {
            dst[i] ^= mul (src[i], m);
        }
    } else {
        for (i = 0; i < chunks; i++) {
            dst[i] = mul (src[i], m);
        }
    }
}

void gf32_pmull_state::mul_region_region (const gf_32_t *src1,
                                          const gf_32_t *src2,
                                          gf_32_t *dst,
                                          uint64_t bytes,
                                          bool accum) const {

    uint64_t i;
    uint64_t chunks = bytes >> 2ULL;
    if (accum) {
        for (i = 0; i < chunks; i++) {
            dst[i] ^= mul (src1[i], src2[i]);
        }
    } else {
        for (i = 0; i < chunks; i++) {
            dst[i] = mul (src1[i], src2[i]);
        }
    }
}

gf_32_t gf32_pmull_state::dotproduct (const gf_32_t *src1,
                                      const gf_32_t *src2,
                                      uint64_t n_elements,
                                      uint64_t stride) const {
    gf_32_t dp = (gf_32_t)0;
    for (uint64_t i = 0; i < n_elements; ++i) {
        dp = add (dp, mul (*src1, *src2));
        src1 += stride;
        src2 += stride;
    }
    return dp;
}
