
/**********************************************************************************
*                                                                                 *
*     Gferasure was developed by the Center for Research in Storage Systems       *
*                         http://www.crss.ucsc.edu/                               *
*                                                                                 *
* BSD 3-Clause License                                                            *
*                                                                                 *
* Copyright (c) 2016, Regents of the University of California                     *
*                                                                                 *
* All rights reserved.                                                            *
*                                                                                 *
* Redistribution and use in source and binary forms, with or without              *
* modification, are permitted provided that the following conditions are met:     *
*                                                                                 *
* 1. Redistributions of source code must retain the above copyright notice,       *
* this list of conditions and the following disclaimer.                           *
*                                                                                 *
* 2. Redistributions in binary form must reproduce the above copyright            *
* notice, this list of conditions and the following disclaimer in the             *
* documentation and/or other materials provided with the distribution.            *
*                                                                                 *
* 3. Neither the name of the copyright holder nor the names of its contributors   *
* may be used to endorse or promote products derived from this software without   *
* specific prior written permission.                                              *
*                                                                                 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"     *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE       *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  *
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    *
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      *
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      *
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   *
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            *
*                                                                                 *
**********************************************************************************/
#include "gf_random.h"

#ifdef ARCH_RDRAND_X86
#else
#include <random>
#endif

uint64_t gf_random_u64 (void) {

    uint64_t v;

#ifdef ARCH_RDRAND_X86
    // int _rdrand_u64_step(unsigned __int64 *);
    // Bull Mountain, intrinsic to generate rand store in v
    __asm__ volatile("rdrand %0" : "=r"(v));
#else
    // For devices without hardware support for rand gen.
    std::mt19937 rng;
    rng.seed (std::random_device () ());
    std::uniform_int_distribution< uint64_t > uint_dist;
    v = uint_dist (rng);
#endif
    return (v);
}

// returns random value based on passed in GF's field size [0, 2^n-1]
// returns a random unsigned interger from 0-255
template <> gf_8_t gf_random_val< gf_8_t > () {
    return (gf_8_t) (gf_random_u64 () % GF8_FIELD_SIZE);
}

// returns a random unsigned integer from 0-65535
template <> gf_16_t gf_random_val< gf_16_t > () {
    return (gf_16_t) (gf_random_u64 () % GF16_FIELD_SIZE);
}

template <> gf_32_t gf_random_val< gf_32_t > () {
    return (gf_32_t) (gf_random_u64 () % GF32_FIELD_SIZE);
}

// returns a random unsigned integer from 0-65535
template <> gf_64_t gf_random_val< gf_64_t > () { return (gf_64_t) (gf_random_u64 ()); }

void gf_random_fill (void *buf, uint64_t len) {
    uint64_t *b = (uint64_t *)buf;
    uint8_t *b8;

    for (uint64_t i = 0; i < len; i += sizeof (uint64_t), b++) {
        *b = gf_random_u64 ();
    }
    for (b8 = (uint8_t *)b; b8 < (uint8_t *)buf + len; b++) {
        *b8 = gf_random_val< gf_8_t > ();
    }
}
