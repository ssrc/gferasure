
/**********************************************************************************
*                                                                                 *
*     Gferasure was developed by the Center for Research in Storage Systems       *
*                         http://www.crss.ucsc.edu/                               *
*                                                                                 *
* BSD 3-Clause License                                                            *
*                                                                                 *
* Copyright (c) 2016, Regents of the University of California                     *
*                                                                                 *
* All rights reserved.                                                            *
*                                                                                 *
* Redistribution and use in source and binary forms, with or without              *
* modification, are permitted provided that the following conditions are met:     *
*                                                                                 *
* 1. Redistributions of source code must retain the above copyright notice,       *
* this list of conditions and the following disclaimer.                           *
*                                                                                 *
* 2. Redistributions in binary form must reproduce the above copyright            *
* notice, this list of conditions and the following disclaimer in the             *
* documentation and/or other materials provided with the distribution.            *
*                                                                                 *
* 3. Neither the name of the copyright holder nor the names of its contributors   *
* may be used to endorse or promote products derived from this software without   *
* specific prior written permission.                                              *
*                                                                                 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"     *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE       *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  *
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    *
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      *
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      *
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   *
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            *
*                                                                                 *
**********************************************************************************/
//
// gf_erasure.cpp
//
// Routines for doing erasure coding.

#include "gf_erasure.h"
#include "gf_matrix.h"
#include <stdio.h>
#include <string.h>

template < class gf_w, class gf_state > void gf_erasure< gf_w, gf_state >::setup () {
    gf_w share_ids[n_data + n_ecc];
    // otherwise warning of unused variable
    memset (share_ids, 0, (n_data + n_ecc) * sizeof (gf_w));
    switch (ecc_type) {
    case ECC_CAUCHY_SYSTEMATIC:
        generator.set_cauchy (true);
        break;
    case ECC_CAUCHY:
        generator.set_cauchy (false);
        break;
    case ECC_VANDERMONDE:
        for (unsigned i = 0; i < n_data + n_ecc; ++i) {
            share_ids[i] = (gf_w)i;
        }
        generator.set_vandermonde ();
        break;
    case ECC_PQ:
        break;
    }
#ifdef DEBUG
    generator.print ();
#endif
}

//
// Generate coded result by applying the generator matrix to the input data.
//
template < class gf_w, class gf_state >
void gf_erasure< gf_w, gf_state >::generate (const gf_w *data[],
                                             gf_w *result[],
                                             uint64_t n_bytes) const {
    generator.apply (data, result, n_bytes);
}

//
// Generate coded result by applying the generator matrix to the input data.
// This generate will also take in the stripe size to multiply regions
//
template < class gf_w, class gf_state >
void gf_erasure< gf_w, gf_state >::generate (const gf_w *data[],
                                             gf_w *result[],
                                             uint64_t n_bytes,
                                             uint64_t stripe) const {
    generator.apply (data, result, n_bytes, stripe);
}

template < class gf_w, class gf_state >
void gf_erasure< gf_w, gf_state >::generate (const gf_w *data[],
                                             gf_w *result[],
                                             uint64_t n_bytes,
                                             bool cache,
                                             uint64_t stripe) const {
    generator.apply (data, result, n_bytes, cache, stripe);
}

//
// Generate a rebuild matrix by copying relevant rows from the generator into the partial,
// and then inverting it into the recovery matrix.
//
template < class gf_w, class gf_state >
bool gf_erasure< gf_w, gf_state >::config_rebuild (const gf_w in_ids[],
                                                   gf_matrix< gf_w, gf_state > *recover) {
    int res;

    if (!recover) {
        recover = &internal_recover;
    }
    for (unsigned r = 0; r < n_data; ++r) {
        partial.copy_row (r, generator, in_ids[r]);
    }
    res = partial.invert (recover);
    return (res == 0);
}

template < class gf_w, class gf_state >
bool gf_erasure< gf_w, gf_state >::rebuild (const gf_w *in[],
                                            gf_w *out[],
                                            uint64_t n_bytes,
                                            const gf_matrix< gf_w, gf_state > *recover) const {
    if (!recover) {
        recover = &internal_recover;
    }
    recover->apply (in, out, n_bytes);
    return true;
}

#include "gf_erasure_fields.h"
