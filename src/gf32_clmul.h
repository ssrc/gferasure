
/**********************************************************************************
*                                                                                 *
*     Gferasure was developed by the Center for Research in Storage Systems       *
*                         http://www.crss.ucsc.edu/                               *
*                                                                                 *
* BSD 3-Clause License                                                            *
*                                                                                 *
* Copyright (c) 2016, Regents of the University of California                     *
*                                                                                 *
* All rights reserved.                                                            *
*                                                                                 *
* Redistribution and use in source and binary forms, with or without              *
* modification, are permitted provided that the following conditions are met:     *
*                                                                                 *
* 1. Redistributions of source code must retain the above copyright notice,       *
* this list of conditions and the following disclaimer.                           *
*                                                                                 *
* 2. Redistributions in binary form must reproduce the above copyright            *
* notice, this list of conditions and the following disclaimer in the             *
* documentation and/or other materials provided with the distribution.            *
*                                                                                 *
* 3. Neither the name of the copyright holder nor the names of its contributors   *
* may be used to endorse or promote products derived from this software without   *
* specific prior written permission.                                              *
*                                                                                 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"     *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE       *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  *
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    *
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      *
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      *
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   *
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            *
*                                                                                 *
**********************************************************************************/
//
// gf32_clmul.h
//
// 32-bit Galois field using SSSE3.
//

#pragma once

#include "gf_types.h"
#include "gfw_shiftadd.h"

#include "gf_random.h"

struct alignas (16) gf32_clmul_state {

    gf32_clmul_state (gf_32_t prim_poly_ = gf32_default_primpoly, gf_32_t alpha_ = 1)
    : prim_poly (prim_poly_), shiftadd_state (prim_poly_, alpha_) {}


    gf_32_t add (gf_32_t a, gf_32_t b) const { return ((gf_32_t) (a ^ b)); }
    gf_32_t sub (gf_32_t a, gf_32_t b) const { return ((gf_32_t) (a ^ b)); }
    gf_32_t inv (gf_32_t a) const { return shiftadd_state.inv (a); }
    gf_32_t pow (gf_32_t a, uint32_t b) const { return shiftadd_state.pow (a, b); }
    gf_32_t mul (gf_32_t a, gf_32_t b) const;
    gf_32_t div (gf_32_t a, gf_32_t b) const { return (mul (a, inv (b))); }

    void mul_region (const gf_32_t *src,
                     gf_32_t *dst,
                     uint64_t bytes,
                     gf_32_t m,
                     bool accum = false,
                     void *mult_table = NULL,
                     bool mult_table_valid = false) const;

    void mul_region_region (const gf_32_t *src1,
                            const gf_32_t *src2,
                            gf_32_t *dst,
                            uint64_t bytes,
                            bool accum = false) const;

    gf_32_t dotproduct (const gf_32_t *src1,
                        const gf_32_t *src2,
                        uint64_t n_elements,
                        uint64_t stride = 1) const;

    uint64_t field_size () const { return GF32_FIELD_SIZE; }
    uint64_t field_max () const { return (field_size () - 1); }
    unsigned mult_table_size () const;

    private:
    uint32_t prim_poly;
    gf32_shiftadd_state shiftadd_state;
};
