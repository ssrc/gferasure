
/**********************************************************************************
*                                                                                 *
*     Gferasure was developed by the Center for Research in Storage Systems       *
*                         http://www.crss.ucsc.edu/                               *
*                                                                                 *
* BSD 3-Clause License                                                            *
*                                                                                 *
* Copyright (c) 2016, Regents of the University of California                     *
*                                                                                 *
* All rights reserved.                                                            *
*                                                                                 *
* Redistribution and use in source and binary forms, with or without              *
* modification, are permitted provided that the following conditions are met:     *
*                                                                                 *
* 1. Redistributions of source code must retain the above copyright notice,       *
* this list of conditions and the following disclaimer.                           *
*                                                                                 *
* 2. Redistributions in binary form must reproduce the above copyright            *
* notice, this list of conditions and the following disclaimer in the             *
* documentation and/or other materials provided with the distribution.            *
*                                                                                 *
* 3. Neither the name of the copyright holder nor the names of its contributors   *
* may be used to endorse or promote products derived from this software without   *
* specific prior written permission.                                              *
*                                                                                 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"     *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE       *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  *
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    *
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      *
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      *
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   *
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            *
*                                                                                 *
**********************************************************************************/
/*
 * gf64_pmull.cpp
 *
 * Routines for 64-bit Galois fields using NEON.
 *
 */

#include <arm_neon.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "gf64_pmull.h"

typedef poly16x8_t __v16p;
typedef poly8x8_t __v8x8p;
typedef uint8x8_t __v8x8u;
typedef uint8x16_t __v8u;
typedef uint16x8_t __v16u;
typedef uint64x2_t __v64u;

static inline __v16u mul128 (uint64_t a, uint64_t b) {
    return vreinterpretq_u16_u64 ((uint64x2_t)vmull_p64 ((poly64_t)a, (poly64_t)b));
}

gf_64_t gf64_pmull_state::mul (gf_64_t a64, gf_64_t b64) const {
    uint64_t pp = prim_poly & 0xffffffffULL;
    uint64x1_t v;
    uint16x8_t w, result;

    result = mul128 ((uint64_t)a64, (uint64_t)b64);
    v = vreinterpret_u64_u32 (vset_lane_u32 (0, vreinterpret_u32_u16 (vget_high_u16 (result)), 0));
    w = mul128 (pp, vget_lane_u64 (v, 0));

    result = veorq_u16 (result, w);
    v = vreinterpret_u64_u32 (vset_lane_u32 (0, vreinterpret_u32_u16 (vget_high_u16 (result)), 1));
    w = mul128 (pp, vget_lane_u64 (v, 0));

    result = veorq_u16 (result, w);
    return (gf_64_t)vget_low_u16 (result);
}

unsigned gf64_pmull_state::mult_table_size () const { return (0); }

void gf64_pmull_state::mul_region (const gf_64_t *src,
                                   gf_64_t *dst,
                                   uint64_t bytes,
                                   gf_64_t m,
                                   bool accum,
                                   void *mult_table,
                                   bool mult_table_valid) const {
    if (m == (gf_64_t)0) {
        if (!accum) {
            memset (dst, 0, bytes);
        }
        return;
    } else if (m == (gf_64_t)1) {
        if (!accum) {
            memcpy (dst, src, bytes);
        } else {
            uint64_t chunks = bytes >> 3ULL;
            for (uint64_t i = 0; i < chunks; i += 2) {
                __v64u x = vld1q_u64 (src + i);
                __v64u y = vld1q_u64 (dst + i);
                vst1q_u64 (dst + i, veorq_u64 (x, y));
            }
        }
        return;
    }

    // ignore compiler unused warning
    (void)mult_table;
    (void)mult_table_valid;

    uint64_t i;
    uint64_t chunks = bytes >> 3ULL;

    uint64x1_t x, y;
    if (accum) {
        for (i = 0; i < chunks; i++) {
            x = vld1_u64 (src + i);
            y = vld1_u64 (dst + i);
            vst1_u64 (dst + i, veor_u64 (vcreate_u64 (mul (vget_lane_u64 (x, 0), m)), y));
        }
    } else {
        for (i = 0; i < chunks; i++) {
            x = vld1_u64 (src + i);
            vst1_u64 (dst + i, vcreate_u64 (mul (vget_lane_u64 (x, 0), m)));
        }
    }
}

void gf64_pmull_state::mul_region_region (const gf_64_t *src1,
                                          const gf_64_t *src2,
                                          gf_64_t *dst,
                                          uint64_t bytes,
                                          bool accum) const {

    uint64_t i;
    uint64_t chunks = bytes >> 3ULL;

    if (accum) {
        uint64x1_t x, y, z;
        for (i = 0; i < chunks; i++) {
            x = vld1_u64 (src1 + i);
            y = vld1_u64 (src2 + i);
            z = vld1_u64 (dst + i);
            vst1_u64 (dst + i,
                      veor_u64 (vcreate_u64 (mul (vget_lane_u64 (x, 0), vget_lane_u64 (y, 0))), z));
        }
    } else {
        uint64x1_t x, y;
        for (i = 0; i < chunks; i++) {
            x = vld1_u64 (src1 + i);
            y = vld1_u64 (src2 + i);
            vst1_u64 (dst + i, vcreate_u64 (mul (vget_lane_u64 (x, 0), vget_lane_u64 (y, 0))));
        }
    }
}

gf_64_t gf64_pmull_state::dotproduct (const gf_64_t *src1,
                                      const gf_64_t *src2,
                                      uint64_t n_elements,
                                      uint64_t stride) const {
    gf_64_t dp = (gf_64_t)0;
    for (uint64_t i = 0; i < n_elements; ++i) {
        dp = add (dp, mul (*src1, *src2));
        src1 += stride;
        src2 += stride;
    }
    return dp;
}
