
/**********************************************************************************
*                                                                                 *
*     Gferasure was developed by the Center for Research in Storage Systems       *
*                         http://www.crss.ucsc.edu/                               *
*                                                                                 *
* BSD 3-Clause License                                                            *
*                                                                                 *
* Copyright (c) 2016, Regents of the University of California                     *
*                                                                                 *
* All rights reserved.                                                            *
*                                                                                 *
* Redistribution and use in source and binary forms, with or without              *
* modification, are permitted provided that the following conditions are met:     *
*                                                                                 *
* 1. Redistributions of source code must retain the above copyright notice,       *
* this list of conditions and the following disclaimer.                           *
*                                                                                 *
* 2. Redistributions in binary form must reproduce the above copyright            *
* notice, this list of conditions and the following disclaimer in the             *
* documentation and/or other materials provided with the distribution.            *
*                                                                                 *
* 3. Neither the name of the copyright holder nor the names of its contributors   *
* may be used to endorse or promote products derived from this software without   *
* specific prior written permission.                                              *
*                                                                                 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"     *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE       *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  *
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    *
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      *
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      *
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   *
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            *
*                                                                                 *
**********************************************************************************/
//
// gf16_log.h
// 8-bit Galois field using log tables.
//

#pragma once

#include "gf_random.h"
#include "gf_types.h"
#include <stdio.h>

// This will be the gf_state passed into gf_w_state
// for 8 bit fields.
struct alignas (32) gf16_gather_state {

    // define primitive polynomial (irreducible)
    gf16_gather_state (gf_16_t prim_poly = gf16_default_primpoly, gf_16_t alpha = 1);

    // single GF operand
    gf_16_t log (gf_16_t a) const { return (this->log_tbl[a]); }
    gf_16_t antilog (gf_16_t a) const { return (this->antilog_tbl[a]); }
    gf_16_t inv (gf_16_t a) const;
    gf_16_t pow (gf_16_t a, uint32_t b) const;

    // GF Arithemtic
    gf_16_t add (gf_16_t a, gf_16_t b) const { return ((gf_16_t) (a ^ b)); }
    gf_16_t sub (gf_16_t a, gf_16_t b) const { return ((gf_16_t) (a ^ b)); }

    gf_16_t mul (gf_16_t a, gf_16_t b) const {
        // if either a or b is 0 we cannot take log of undefined
        // otherwise take log of a+b, then antilog(a+b) to multiply
        return ((a == 0 || b == 0) ? 0 : antilog_tbl[(uint32_t)log (a) + (uint32_t)log (b)]);
    }
    // same for divide, cannot log of 0, otherwise subtract
    // and take the antilog
    gf_16_t div (gf_16_t a, gf_16_t b) const {
        return ((a == 0 || b == 0) ?
                0 :
                //-1 because field is shifted, and here we need to index, so 255
                antilog ((gf_16_t) (((GF16_FIELD_SIZE - 1) + (int)log (a) - (int)log (b)) %
                                    (GF16_FIELD_SIZE - 1))));
    }

    void mul_region (const gf_16_t *src,
                     gf_16_t *dst,
                     uint64_t bytes,
                     gf_16_t m,
                     bool accum = false,
                     void *mult_table = NULL,
                     bool mult_table_valid = false) const;
    void mul_region_region (const gf_16_t *src1,
                            const gf_16_t *src2,
                            gf_16_t *dst,
                            uint64_t bytes,
                            bool accum = false) const;
    gf_16_t dotproduct (const gf_16_t *src1,
                        const gf_16_t *src2,
                        uint64_t n_elements,
                        uint64_t stride = 1) const;
    bool rs_encode (gf_16_t **const data,
                    unsigned n_data,
                    gf_16_t **parity,
                    unsigned n_parity,
                    uint64_t length) const;

    uint64_t field_size () const { return GF16_FIELD_SIZE; }
    uint64_t field_max () const { return (field_size () - 1); }
    unsigned mult_table_size () const;

    private:
    // log and antilog tables
    gf_16_t log_tbl[GF16_FIELD_SIZE];
    gf_16_t antilog_tbl[GF16_FIELD_SIZE * 2];

    // GF primitives
    gf_16_t alpha;
    uint32_t prim_poly;
};
