/**********************************************************************************
*                                                                                 *
*     Gferasure was developed by the Center for Research in Storage Systems       *
*                         http://www.crss.ucsc.edu/                               *
*                                                                                 *
* BSD 3-Clause License                                                            *
*                                                                                 *
* Copyright (c) 2016, Regents of the University of California                     *
*                                                                                 *
* All rights reserved.                                                            *
*                                                                                 *
* Redistribution and use in source and binary forms, with or without              *
* modification, are permitted provided that the following conditions are met:     *
*                                                                                 *
* 1. Redistributions of source code must retain the above copyright notice,       *
* this list of conditions and the following disclaimer.                           *
*                                                                                 *
* 2. Redistributions in binary form must reproduce the above copyright            *
* notice, this list of conditions and the following disclaimer in the             *
* documentation and/or other materials provided with the distribution.            *
*                                                                                 *
* 3. Neither the name of the copyright holder nor the names of its contributors   *
* may be used to endorse or promote products derived from this software without   *
* specific prior written permission.                                              *
*                                                                                 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"     *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE       *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  *
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    *
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      *
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      *
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   *
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            *
*                                                                                 *
**********************************************************************************/


/*
 * gf16_ssse3_shiftadd.cpp
 *
 * Routines for 16-bit Galois fields using shift-add under SSSE3.
 *
 */

#include "gf16_ssse3_shiftadd.h"
#include <string.h>
#include <x86intrin.h>

void gf16_ssse3_shiftadd_state::setup () {}


static inline void
mul_vec_by_vec_rnd (__m128i &r, __m128i v1, __m128i &v2, __m128i poly, __m128i zero, int pos) {
    __m128i t1;

    // Shift mask bit to high-order
    t1 = _mm_slli_epi16 (v1, pos);
    // Add into accumulator (using XOR) if bit in multiplicand is non-zero
    r = _mm_xor_si128 (r, _mm_blend_epi16 (zero, v2, t1));
    // Multiply by 2
    t1 = _mm_blend_epi16 (zero, poly, v2);
    v2 = _mm_xor_si128 (_mm_add_epi16 (v2, v2), t1);
}

static inline __m128i mul_vecs_by_vecs (__m128i v1, __m128i v1, __m128i v2, __m128i pp) {
    __m128i r, zero;

    r = _mm_setzero_si128 ();
    zero = _mm_setzero_si128 ();

    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 15);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 14);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 13);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 12);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 11);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 10);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 9);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 8);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 7);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 6);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 5);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 4);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 3);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 2);
    mul_vec_by_vec_rnd (r, v1, v2, pp, zero, 1);
    // Handle the last round
    r = _mm_xor_si128 (r, _mm_blend_epi16 (zero, v2, v1));

    return r;
}

void gf16_ssse3_shiftadd_state::mul_region (gf_16_t *const src,
                                            gf_16_t *dst,
                                            uint64_t bytes,
                                            gf_16_t multiplier,
                                            bool accum) const {

    if (multiplier == (gf_16_t)0) {
        if (!accum) {
            memset (dst, 0, bytes);
        }
        return;
    }

    __m128i mul = _mm_set1_epi16 ((int16_t)multiplier);
    __m128i pp = _mm_set1_epi16 ((int16_t)prim_poly);
    __m128i v;
    uint64_t i;
    uint64_t chunks = bytes >> 4ULL;

    if (accum) {
        for (i = 0; i < chunks; ++i) {
            // load a 128 bit value, it does not need to be alinged _mm_loadu_si128
            v = _mm_loadu_si128 (((__m128i *)src) + i);
            v = _mm_xor_si128 (_mm_loadu_si128 (((__m128i *)dst) + i), mul_vec_by_vec (v, mul, pp));
            // store the value back into dst
            _mm_storeu_si128 (((__m128i *)dst) + i, v);
        }
    } else {
        for (i = 0; i < chunks; ++i) {
            v = _mm_loadu_si128 (((__m128i *)src) + i);
            v = mul_vec_by_vec (v, mul, pp);
            _mm_storeu_si128 (((__m128i *)dst) + i, v);
        }
    }

    for (i = chunks << 4ULL; i < bytes; i += 2) {
        dst[i] = mul (src[i], multiplier) ^ (accum ? dst[i] : (gf_16_t)0);
    }
}

void gf16_ssse3_shiftadd_state::mul_region_region (gf_16_t *const buf1,
                                                   gf_16_t *const buf2,
                                                   gf_16_t *dst,
                                                   uint64_t bytes,
                                                   bool accum) const {
    uint64_t i;
    uint64_t chunks = bytes >> 4ULL;
    __m128i v1, v2, r, pp;

    pp = _mm_set1_epi16 ((int16_t)prim_poly);
    if (accum) {
        for (i = 0; i < chunks; ++i) {
            v1 = _mm_loadu_si128 (((__m128i *)buf1) + i);
            v2 = _mm_loadu_si128 (((__m128i *)buf2) + i);
            r = _mm_xor_si128 (_mm_loadu_si128 (((__m128i *)dst) + i), mul_vec_by_vec (v1, v2, pp));
            _mm_storeu_si128 (((__m128i *)dst) + i, r);
        }
        for (i = (chunks * 16); i < bytes; ++i) {
            dst[i] = add (dst[i], mul (buf1[i], buf2[i]));
        }
    } else {
        for (i = 0; i < chunks; ++i) {
            v1 = _mm_loadu_si128 (((__m128i *)buf1) + i);
            v2 = _mm_loadu_si128 (((__m128i *)buf2) + i);
            r = mul_vec_by_vec (v1, v2, pp);
            _mm_storeu_si128 (((__m128i *)dst) + i, r);
        }
        if (!!(bytes & (sizeof (__m128i) - 1))) {
            // Handle the last 16 bytes.  Probably faster to just do 16 using unaligned access
            v1 = _mm_loadu_si128 ((__m128i *)(buf1 + bytes - sizeof (__m128i)));
            v2 = _mm_loadu_si128 ((__m128i *)(buf2 + bytes - sizeof (__m128i)));
            r = mul_vec_by_vec (v1, v2, pp);
            _mm_storeu_si128 ((__m128i *)(buf1 + bytes - sizeof (__m128i)), r);
        }
    }
}

//
// Since we're not using this for much, we want to make it more memory-efficient rather than
// speed-efficient.  We don't precompute the inverse table; we can just look up inverses
// using the log / antilog tables instead.
//
::gf16_ssse3_shiftadd_state (gf_16_t prim_poly, gf_16_t alpha) {

    uint32_t b, i;

    this->alpha = alpha;
    this->prim_poly = 0x100 | (uint32_t)prim_poly;

    for (b = this->alpha, i = 0; i < GF16_FIELD_SIZE - 1; i++) {
        log_tbl[b] = (gf_16_t)i;
        antilog_tbl[i] = (gf_16_t)b;
        antilog_tbl[i + GF16_FIELD_SIZE - 1] = (gf_16_t)b;
        b <<= 1;
        if (b & GF16_FIELD_SIZE) {
            b ^= this->prim_poly;
        }
    }
}
