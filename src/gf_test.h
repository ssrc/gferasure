
/**********************************************************************************
*                                                                                 *
*     Gferasure was developed by the Center for Research in Storage Systems       *
*                         http://www.crss.ucsc.edu/                               *
*                                                                                 *
* BSD 3-Clause License                                                            *
*                                                                                 *
* Copyright (c) 2016, Regents of the University of California                     *
*                                                                                 *
* All rights reserved.                                                            *
*                                                                                 *
* Redistribution and use in source and binary forms, with or without              *
* modification, are permitted provided that the following conditions are met:     *
*                                                                                 *
* 1. Redistributions of source code must retain the above copyright notice,       *
* this list of conditions and the following disclaimer.                           *
*                                                                                 *
* 2. Redistributions in binary form must reproduce the above copyright            *
* notice, this list of conditions and the following disclaimer in the             *
* documentation and/or other materials provided with the distribution.            *
*                                                                                 *
* 3. Neither the name of the copyright holder nor the names of its contributors   *
* may be used to endorse or promote products derived from this software without   *
* specific prior written permission.                                              *
*                                                                                 *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"     *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE       *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE  *
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE    *
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL      *
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR      *
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER      *
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,   *
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.            *
*                                                                                 *
**********************************************************************************/


#include "gf_erasure.h" //included to test erasure codes
#include "gf_random.h"
#include "gf_secretsplit.h" // included to test secret split
#include "gf_w.h"

struct timer {
    double start_time;
    double split_time;
    void start () { start_time = split_time = gettime_d (); }

    double split () {
        double new_t = gettime_d ();
        double elapsed = new_t - split_time;
        split_time = new_t;
        return elapsed;
    }
    double stop () {
        double new_t = gettime_d ();
        return new_t - start_time;
    }
    static double gettime_d () {
        struct timeval tv;
        gettimeofday (&tv, NULL);
        return (double)tv.tv_sec + (double)tv.tv_usec * 1e-6;
    }
};

template < class gf_w, class gf_state >
double gf_w_state< gf_w, gf_state >::time_trial (gf_w *buf1,
                                                 gf_w *buf2,
                                                 gf_w *buf3,
                                                 uint64_t len,
                                                 char which_test,
                                                 unsigned n_trials) {
    gf_w dotprod = (gf_w)0;
    struct timer tmr;
    double elapsed = 0.0;

    tmr.start ();
    for (unsigned i = 0; i < n_trials; ++i) {
        // Assign new values so we force execution of the test
        buf2[0] = gf_random_val< gf_w > ();
        switch (which_test) {
        case 'm':
            mul_region (buf1, buf3, len, buf2[0]);
            break;
        case 'r':
            mul_region_region (buf1, buf2, buf3, len);
            break;
        case 'd':
            dotprod ^= dotproduct (buf1, buf2, len / sizeof (gf_w));
            break;
        default:
            break;
        }
    }
    elapsed += tmr.stop ();
    return (elapsed);
}

template < class gf_w, class gf_state >
double timed_erasure_encode (unsigned n_data,
                             unsigned n_ecc,
                             uint64_t n_bytes,
                             unsigned n_tests,
                             unsigned n_stripe = 0,
                             unsigned ecc_type = ECC_CAUCHY_SYSTEMATIC) {
    uint64_t n_words = n_bytes / sizeof (gf_w);
    gf_erasure< gf_w, gf_state > kernel (n_data, n_ecc, ecc_type);
    gf_w *data[n_data];
    gf_w *out_bufs[n_data + n_ecc];
    gf_w *space = new gf_w[n_words * (3 * n_data + n_ecc)];
    unsigned i;

    for (i = 0; i < n_data; ++i, space += n_words) {
        data[i] = space;
        gf_random_fill (data[i], n_bytes);
    }

    // Allocate space for stored information.  This can be up to n_data + n_ecc because
    // some codes are non-systematic.
    for (i = 0; i < n_data + n_ecc; ++i, space += n_words) {
        out_bufs[i] = space;
    }

    struct timer tmr;
    double elapsed = -0.0;

    if (!n_stripe) {
        tmr.start ();
        for (unsigned i = 0; i < n_tests; ++i) {
            // Generate the coded data
            kernel.generate ((const gf_w **)data, out_bufs, n_bytes);
        }
        elapsed = tmr.stop ();
        return (elapsed);
    } else {
        tmr.start ();
        for (unsigned i = 0; i < n_tests; ++i) {
            // Generate the coded data
            // Cache the matrix
            kernel.generate ((const gf_w **)data, out_bufs, n_bytes, true, n_stripe);
            // Do not use the cache matrix
            /* kernel.generate ((const gf_w **)data, out_bufs, n_bytes, n_stripe); */
        }
        elapsed = tmr.stop ();
        return (elapsed);
    }
}


template < class gf_w, class gf_state >
double timed_secret_split (unsigned n_trials,
                           uint64_t n_bytes,
                           unsigned n_shares,
                           unsigned n_min,
                           unsigned n_random_ids) {
    struct timer tmr;
    double elapsed = 0.0;
    split_secret< gf_w, gf_state > sec_sa8 (n_bytes, n_shares, n_min);
    tmr.start ();
    for (unsigned i = 0; i < n_trials; ++i) {
        // returns non-void type, chuck away
        sec_sa8.sanity_check (n_trials, n_random_ids);
    }
    elapsed = tmr.stop ();
    return elapsed;
}

struct secret_split_info {
    const char *field_name;
    uint64_t n_bytes_field_unittest;
    unsigned n_tests_field_unittest;
    double (*perf) (unsigned n_shares,
                    unsigned n_min,
                    uint64_t bytes,
                    unsigned tests,
                    unsigned n_random_ids);
};

static struct secret_split_info secret_test_fields[] = {
    { "gf8_shiftadd", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_8_t, gf8_shiftadd_state > (shares, n_min, bytes, tests,
                                                                   n_random_ids);
      } },
    { "gf16_shiftadd", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_16_t, gf16_shiftadd_state > (shares, n_min, bytes, tests,
                                                                     n_random_ids);
      } },
    { "gf32_shiftadd", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_32_t, gf32_shiftadd_state > (shares, n_min, bytes, tests,
                                                                     n_random_ids);
      } },
    { "gf64_shiftadd", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_64_t, gf64_shiftadd_state > (shares, n_min, bytes, tests,
                                                                     n_random_ids);
      } },
    { "gf8_log", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_8_t, gf8_log_state > (shares, n_min, bytes, tests,
                                                              n_random_ids);
      } },
    { "gf16_log", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_16_t, gf16_log_state > (shares, n_min, bytes, tests,
                                                                n_random_ids);
      } },
#ifdef GF_SSSE3
    { "gf8_ssse3", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_8_t, gf8_ssse3_state > (shares, n_min, bytes, tests,
                                                                n_random_ids);
      } },
    { "gf16_ssse3", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_16_t, gf16_ssse3_state > (shares, n_min, bytes, tests,
                                                                  n_random_ids);
      } },
#endif
#ifdef GF_AVX
    { "gf32_avx_shift2", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_32_t, gf32_avx_shift2_state > (shares, n_min, bytes, tests,
                                                                       n_random_ids);
      } },
#endif // GF_AVX
#ifdef GF_AVX2
    { "gf8_avx2", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_8_t, gf8_avx2_state > (shares, n_min, bytes, tests,
                                                               n_random_ids);
      } },
    { "gf16_avx2", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_16_t, gf16_avx2_state > (shares, n_min, bytes, tests,
                                                                 n_random_ids);
      } },
    { "gf8_gather", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_8_t, gf8_gather_state > (shares, n_min, bytes, tests,
                                                                 n_random_ids);
      } },
    { "gf16_gather", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_16_t, gf16_gather_state > (shares, n_min, bytes, tests,
                                                                   n_random_ids);
      } },
#endif
#ifdef GF_CLMUL
    { "gf8_clmul", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_8_t, gf8_clmul_state > (shares, n_min, bytes, tests,
                                                                n_random_ids);
      } },
    { "gf16_clmul", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_16_t, gf16_clmul_state > (shares, n_min, bytes, tests,
                                                                  n_random_ids);
      } },
    { "gf32_clmul", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_32_t, gf32_clmul_state > (shares, n_min, bytes, tests,
                                                                  n_random_ids);
      } },
    { "gf64_clmul", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_64_t, gf64_clmul_state > (shares, n_min, bytes, tests,
                                                                  n_random_ids);
      } },
#endif
#if defined(GF_ARMV7) || defined(GF_ARMV8)
    { "gf8_vmull", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_8_t, gf8_vmull_state > (shares, n_min, bytes, tests,
                                                                n_random_ids);
      } },
    { "gf16_vmull", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_16_t, gf16_vmull_state > (shares, n_min, bytes, tests,
                                                                  n_random_ids);
      } },
    { "gf32_vmull", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_32_t, gf32_vmull_state > (shares, n_min, bytes, tests,
                                                                  n_random_ids);
      } },
    { "gf64_vmull", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_64_t, gf64_vmull_state > (shares, n_min, bytes, tests,
                                                                  n_random_ids);
      } },
    { "gf8_neon", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_8_t, gf8_neon_state > (shares, n_min, bytes, tests,
                                                               n_random_ids);
      } },
    { "gf16_neon", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_16_t, gf16_neon_state > (shares, n_min, bytes, tests,
                                                                 n_random_ids);
      } },
#endif
#ifdef GF_PMULL
    { "gf8_pmull", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_8_t, gf8_pmull_state > (shares, n_min, bytes, tests,
                                                                n_random_ids);
      } },
    { "gf16_pmull", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_16_t, gf16_pmull_state > (shares, n_min, bytes, tests,
                                                                  n_random_ids);
      } },
    { "gf32_pmull", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_32_t, gf32_pmull_state > (shares, n_min, bytes, tests,
                                                                  n_random_ids);
      } },
    { "gf64_pmull", 1024, 1000,
      [](unsigned shares, unsigned n_min, uint64_t bytes, unsigned tests, unsigned n_random_ids)
      -> double {
          return timed_secret_split< gf_64_t, gf64_pmull_state > (shares, n_min, bytes, tests,
                                                                  n_random_ids);
      } },
#endif
};


struct encode_info {
    const char *field_name;
    uint64_t n_bytes_field_unittest;
    unsigned n_tests_field_unittest;
    double (*perf) (unsigned n_data,
                    unsigned n_ecc,
                    uint64_t n_bytes,
                    unsigned n_tests,
                    unsigned stripe,
                    unsigned ecc_mode);
};

static struct encode_info encode_test_fields[] = {
    { "gf8_shiftadd", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_8_t, gf8_shiftadd_state > (data, ecc, bytes, tests,
                                                                     stripe, ecc_mode);
      } },
    { "gf16_shiftadd", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_16_t, gf16_shiftadd_state > (data, ecc, bytes, tests,
                                                                       stripe, ecc_mode);
      } },
    { "gf32_shiftadd", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_32_t, gf32_shiftadd_state > (data, ecc, bytes, tests,
                                                                       stripe, ecc_mode);
      } },
    { "gf64_shiftadd", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_64_t, gf64_shiftadd_state > (data, ecc, bytes, tests,
                                                                       stripe, ecc_mode);
      } },
    { "gf8_log", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_8_t, gf8_log_state > (data, ecc, bytes, tests, stripe,
                                                                ecc_mode);
      } },
    { "gf16_log", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_16_t, gf16_log_state > (data, ecc, bytes, tests, stripe,
                                                                  ecc_mode);
      } },
#ifdef GF_SSSE3
    { "gf8_ssse3", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_8_t, gf8_ssse3_state > (data, ecc, bytes, tests, stripe,
                                                                  ecc_mode);
      } },
    { "gf16_ssse3", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_16_t, gf16_ssse3_state > (data, ecc, bytes, tests, stripe,
                                                                    ecc_mode);
      } },
#endif
#ifdef GF_AVX
    { "gf32_avx_shift2", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_32_t, gf32_avx_shift2_state > (data, ecc, bytes, tests,
                                                                         stripe, ecc_mode);
      } },
#endif // GF_AVX
#ifdef GF_AVX2
    { "gf8_avx2", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_8_t, gf8_avx2_state > (data, ecc, bytes, tests, stripe,
                                                                 ecc_mode);
      } },
    { "gf16_avx2", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_16_t, gf16_avx2_state > (data, ecc, bytes, tests, stripe,
                                                                   ecc_mode);
      } },
    { "gf8_gather", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_8_t, gf8_gather_state > (data, ecc, bytes, tests, stripe,
                                                                   ecc_mode);
      } },
    { "gf16_gather", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_16_t, gf16_gather_state > (data, ecc, bytes, tests,
                                                                     stripe, ecc_mode);
      } },
#endif
#ifdef GF_CLMUL
    { "gf8_clmul", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_8_t, gf8_clmul_state > (data, ecc, bytes, tests, stripe,
                                                                  ecc_mode);
      } },
    { "gf16_clmul", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_16_t, gf16_clmul_state > (data, ecc, bytes, tests, stripe,
                                                                    ecc_mode);
      } },
    { "gf32_clmul", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_32_t, gf32_clmul_state > (data, ecc, bytes, tests, stripe,
                                                                    ecc_mode);
      } },
    { "gf64_clmul", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_64_t, gf64_clmul_state > (data, ecc, bytes, tests, stripe,
                                                                    ecc_mode);
      } },
#endif
#if defined(GF_ARMV7) || defined(GF_ARMV8)
    { "gf8_vmull", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_8_t, gf8_vmull_state > (data, ecc, bytes, tests, stripe,
                                                                  ecc_mode);
      } },
    { "gf16_vmull", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_16_t, gf16_vmull_state > (data, ecc, bytes, tests, stripe,
                                                                    ecc_mode);
      } },
    { "gf32_vmull", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_32_t, gf32_vmull_state > (data, ecc, bytes, tests, stripe,
                                                                    ecc_mode);
      } },
    { "gf64_vmull", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_64_t, gf64_vmull_state > (data, ecc, bytes, tests, stripe,
                                                                    ecc_mode);
      } },
    { "gf8_neon", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_8_t, gf8_neon_state > (data, ecc, bytes, tests, stripe,
                                                                 ecc_mode);
      } },
    { "gf16_neon", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_16_t, gf16_neon_state > (data, ecc, bytes, tests, stripe,
                                                                   ecc_mode);
      } },
#endif
#ifdef GF_PMULL
    { "gf8_pmull", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_8_t, gf8_pmull_state > (data, ecc, bytes, tests, stripe,
                                                                  ecc_mode);
      } },
    { "gf16_pmull", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_16_t, gf16_pmull_state > (data, ecc, bytes, tests, stripe,
                                                                    ecc_mode);
      } },
    { "gf32_pmull", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_32_t, gf32_pmull_state > (data, ecc, bytes, tests, stripe,
                                                                    ecc_mode);
      } },
    { "gf64_pmull", 1024, 1000,
      [](unsigned data,
         unsigned ecc,
         uint64_t bytes,
         unsigned tests,
         unsigned stripe,
         unsigned ecc_mode) -> double {
          return timed_erasure_encode< gf_64_t, gf64_pmull_state > (data, ecc, bytes, tests, stripe,
                                                                    ecc_mode);
      } },
#endif
};


static gf_w_state< gf_8_t, gf8_shiftadd_state > gf8_shiftadd_test (gf8_default_primpoly);
static gf_w_state< gf_16_t, gf16_shiftadd_state > gf16_shiftadd_test (gf16_default_primpoly);
static gf_w_state< gf_32_t, gf32_shiftadd_state > gf32_shiftadd_test (gf32_default_primpoly);
static gf_w_state< gf_64_t, gf64_shiftadd_state > gf64_shiftadd_test (gf64_default_primpoly);
#ifdef GF_LOG
static gf_w_state< gf_8_t, gf8_log_state > gf8_log_test (gf8_default_primpoly);
static gf_w_state< gf_16_t, gf16_log_state > gf16_log_test (gf16_default_primpoly);
#endif

#ifdef GF_SHIFT2
static gf_w_state< gf_8_t, gf8_shift2_state > gf8_shift2_test (gf8_default_primpoly);
static gf_w_state< gf_16_t, gf16_shift2_state > gf16_shift2_test (gf16_default_primpoly);
#endif

#ifdef GF_SSSE3
static gf_w_state< gf_8_t, gf8_ssse3_state > gf8_ssse3_test (gf8_default_primpoly);
static gf_w_state< gf_16_t, gf16_ssse3_state > gf16_ssse3_test (gf16_default_primpoly);
#endif
#ifdef GF_AVX
static gf_w_state< gf_32_t, gf32_avx_shift2_state > gf32_avx_shift2_test (gf32_default_primpoly);
#endif // GF_AVX

#ifdef GF_AVX2
static gf_w_state< gf_8_t, gf8_avx2_state > gf8_avx2_test (gf8_default_primpoly);
static gf_w_state< gf_16_t, gf16_avx2_state > gf16_avx2_test (gf16_default_primpoly);
static gf_w_state< gf_8_t, gf8_gather_state > gf8_gather_test (gf8_default_primpoly);
static gf_w_state< gf_16_t, gf16_gather_state > gf16_gather_test (gf16_default_primpoly);
#endif
#ifdef GF_CLMUL
static gf_w_state< gf_8_t, gf8_clmul_state > gf8_clmul_test (gf8_default_primpoly);
static gf_w_state< gf_16_t, gf16_clmul_state > gf16_clmul_test (gf16_default_primpoly);
static gf_w_state< gf_32_t, gf32_clmul_state > gf32_clmul_test (gf32_default_primpoly);
static gf_w_state< gf_64_t, gf64_clmul_state > gf64_clmul_test (gf64_default_primpoly);
#endif
#if defined(GF_ARMV7) || defined(GF_ARMV8)
static gf_w_state< gf_8_t, gf8_neon_state > gf8_neon_test (gf8_default_primpoly);
static gf_w_state< gf_16_t, gf16_neon_state > gf16_neon_test (gf16_default_primpoly);
static gf_w_state< gf_8_t, gf8_vmull_state > gf8_vmull_test (gf8_default_primpoly);
static gf_w_state< gf_16_t, gf16_vmull_state > gf16_vmull_test (gf16_default_primpoly);
static gf_w_state< gf_32_t, gf32_vmull_state > gf32_vmull_test (gf32_default_primpoly);
static gf_w_state< gf_64_t, gf64_vmull_state > gf64_vmull_test (gf64_default_primpoly);
#endif
#ifdef GF_PMULL
static gf_w_state< gf_8_t, gf8_pmull_state > gf8_pmull_test (gf8_default_primpoly);
static gf_w_state< gf_16_t, gf16_pmull_state > gf16_pmull_test (gf16_default_primpoly);
static gf_w_state< gf_32_t, gf32_pmull_state > gf32_pmull_test (gf32_default_primpoly);
static gf_w_state< gf_64_t, gf64_pmull_state > gf64_pmull_test (gf64_default_primpoly);
#endif


struct galois_info {
    const char *field_name;
    uint64_t n_bytes_field_unittest;
    unsigned n_tests_field_unittest;
    double (*perf) (gf_32_t *buf1,
                    gf_32_t *buf2,
                    gf_32_t *buf3,
                    uint64_t bytes,
                    char which_test,
                    unsigned tests);
};


static struct galois_info test_fields[] = {
    { "gf8_shiftadd", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf8_shiftadd_test.time_trial ((gf_8_t *)buf1, (gf_8_t *)buf2, (gf_8_t *)buf3,
                                               bytes, wt, tests);
      } },
    { "gf16_shiftadd", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf16_shiftadd_test.time_trial ((gf_16_t *)buf1, (gf_16_t *)buf2, (gf_16_t *)buf3,
                                                bytes, wt, tests);
      } },
    { "gf32_shiftadd", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf32_shiftadd_test.time_trial ((gf_32_t *)buf1, (gf_32_t *)buf2, (gf_32_t *)buf3,
                                                bytes, wt, tests);
      } },
    { "gf64_shiftadd", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf64_shiftadd_test.time_trial ((gf_64_t *)buf1, (gf_64_t *)buf2, (gf_64_t *)buf3,
                                                bytes, wt, tests);
      } },
    { "gf8_log", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf8_log_test.time_trial ((gf_8_t *)buf1, (gf_8_t *)buf2, (gf_8_t *)buf3, bytes, wt,
                                          tests);
      } },
    { "gf16_log", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf16_log_test.time_trial ((gf_16_t *)buf1, (gf_16_t *)buf2, (gf_16_t *)buf3, bytes,
                                           wt, tests);
      } },
#ifdef GF_SSSE3
    { "gf8_ssse3", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf8_ssse3_test.time_trial ((gf_8_t *)buf1, (gf_8_t *)buf2, (gf_8_t *)buf3, bytes,
                                            wt, tests);
      } },
    { "gf16_ssse3", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf16_ssse3_test.time_trial ((gf_16_t *)buf1, (gf_16_t *)buf2, (gf_16_t *)buf3,
                                             bytes, wt, tests);
      } },
#endif
#ifdef GF_AVX
    { "gf32_avx_shift2", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf32_shiftadd_test.time_trial ((gf_32_t *)buf1, (gf_32_t *)buf2, (gf_32_t *)buf3,
                                                bytes, wt, tests);
      } },
#endif // GF_AVX
#ifdef GF_AVX2
    { "gf8_avx2", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf8_avx2_test.time_trial ((gf_8_t *)buf1, (gf_8_t *)buf2, (gf_8_t *)buf3, bytes,
                                           wt, tests);
      } },
    { "gf16_avx2", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf16_avx2_test.time_trial ((gf_16_t *)buf1, (gf_16_t *)buf2, (gf_16_t *)buf3,
                                            bytes, wt, tests);
      } },
    { "gf8_gather", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf8_gather_test.time_trial ((gf_8_t *)buf1, (gf_8_t *)buf2, (gf_8_t *)buf3, bytes,
                                             wt, tests);
      } },
    { "gf16_gather", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf16_gather_test.time_trial ((gf_16_t *)buf1, (gf_16_t *)buf2, (gf_16_t *)buf3,
                                              bytes, wt, tests);
      } },
#endif
#ifdef GF_CLMUL
    { "gf8_clmul", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf8_clmul_test.time_trial ((gf_8_t *)buf1, (gf_8_t *)buf2, (gf_8_t *)buf3, bytes,
                                            wt, tests);
      } },
    { "gf16_clmul", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf16_clmul_test.time_trial ((gf_16_t *)buf1, (gf_16_t *)buf2, (gf_16_t *)buf3,
                                             bytes, wt, tests);
      } },
    { "gf32_clmul", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf32_clmul_test.time_trial ((gf_32_t *)buf1, (gf_32_t *)buf2, (gf_32_t *)buf3,
                                             bytes, wt, tests);
      } },
    { "gf64_clmul", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf64_clmul_test.time_trial ((gf_64_t *)buf1, (gf_64_t *)buf2, (gf_64_t *)buf3,
                                             bytes, wt, tests);
      } },
#endif
#if defined(GF_ARMV7) || defined(GF_ARMV8)
    { "gf8_neon", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf8_neon_test.time_trial ((gf_8_t *)buf1, (gf_8_t *)buf2, (gf_8_t *)buf3, bytes,
                                           wt, tests);
      } },
    { "gf16_neon", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf16_neon_test.time_trial ((gf_16_t *)buf1, (gf_16_t *)buf2, (gf_16_t *)buf3,
                                            bytes, wt, tests);
      } },
    { "gf8_vmull", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf8_vmull_test.time_trial ((gf_8_t *)buf1, (gf_8_t *)buf2, (gf_8_t *)buf3, bytes,
                                            wt, tests);
      } },
    { "gf16_vmull", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf16_vmull_test.time_trial ((gf_16_t *)buf1, (gf_16_t *)buf2, (gf_16_t *)buf3,
                                             bytes, wt, tests);
      } },
    { "gf32_vmull", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf32_vmull_test.time_trial ((gf_32_t *)buf1, (gf_32_t *)buf2, (gf_32_t *)buf3,
                                             bytes, wt, tests);
      } },
    { "gf64_vmull", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf64_vmull_test.time_trial ((gf_64_t *)buf1, (gf_64_t *)buf2, (gf_64_t *)buf3,
                                             bytes, wt, tests);
      } },
#endif
#ifdef GF_PMULL
    { "gf8_pmull", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf8_pmull_test.time_trial ((gf_8_t *)buf1, (gf_8_t *)buf2, (gf_8_t *)buf3, bytes,
                                            wt, tests);
      } },
    { "gf16_pmull", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf16_pmull_test.time_trial ((gf_16_t *)buf1, (gf_16_t *)buf2, (gf_16_t *)buf3,
                                             bytes, wt, tests);
      } },
    { "gf32_pmull", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf32_pmull_test.time_trial ((gf_32_t *)buf1, (gf_32_t *)buf2, (gf_32_t *)buf3,
                                             bytes, wt, tests);
      } },
    { "gf64_pmull", 1024, 1000,
      [](gf_32_t *buf1, gf_32_t *buf2, gf_32_t *buf3, uint64_t bytes, char wt, unsigned tests)
      -> double {
          return gf64_pmull_test.time_trial ((gf_64_t *)buf1, (gf_64_t *)buf2, (gf_64_t *)buf3,
                                             bytes, wt, tests);
      } },
#endif
};

unsigned n_fields = sizeof (test_fields) / sizeof (test_fields[0]);
unsigned n_encode_fields = sizeof (encode_test_fields) / sizeof (encode_test_fields[0]);
unsigned n_secret_fields = sizeof (secret_test_fields) / sizeof (secret_test_fields[0]);
