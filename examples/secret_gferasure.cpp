#include <fstream>
#include <iostream>

#include "gf_secretsplit.h"

int main (int argc, char **argv)
{

  // how many shares should we create for threshold scheme
  unsigned num_shares = 5;
  // how many required in order to rebuild secret
  unsigned min_shares = 3;
  // filename
  std::string fi_name ("demo.txt");
  // use the file demo.txt as our secret file
  std::ifstream fi_split (fi_name, std::ifstream::in);
  // store the contents in a string
  std::string fi_contents ((std::istreambuf_iterator< char > (fi_split)),
                           std::istreambuf_iterator< char > ( ));
  // get the size of the string
  unsigned long bytes = fi_contents.size ( );

  // print out settings
  std::cout << "Shares to create: " << num_shares << std::endl;
  std::cout << "Shares to reconstruct: " << min_shares << std::endl;

  // create a shamir secret split using GF8 ShiftAdd
  split_secret< gf_16_t, gf16_shiftadd_state > secret (bytes, num_shares, min_shares);

  // create the secret split shares
  secret.create_shares ((void *)fi_contents.c_str ( ));

  // print the number of shares generated.
  std::cout << "Valid shares: " << secret.get_all_valid_ids ( ) << std::endl;

  unsigned iter = 1;
  while (secret.find_secret ( ))
  {
    // secret.shares[iter].data = (uint8_t*)0;
    secret.delete_share (iter);
    secret.secret_valid = 0;
    std::cout << "Deleting share. Valid shares: " << secret.get_all_valid_ids ( ) << std::endl;
    std::cout << "Reconstruction possible: " << ((secret.find_secret ( )) ? "True" : "False")
              << std::endl;
    iter++;
  }
}
