# **Gferasure** #

Gferasure is a high performance Galois field library for erasure coding and 
algebraic signature computation.  Gferasure implements constant by region
multiplication for erasure coding applications and dot product multiplication
for algebraic signatures along with normal Galois field multiplication.  
Gferasure utilizes Intel's Single Instruction Multiple Data (SIMD) instruction
sets Supplemental Streaming SIMD Extensions 3 (SSSE3), Streaming SIMD 
Extensions 4 (SSE4), and Advanced Vector eXtentions 2 (AVX2) for Intel 
processors along with NEON instructions for ARM processors.  Gferasure 
currently supports template implementations for field sizes GF(2^8), 
GF(2^16), GF(2^32), and GF(2^64).

## Building / Running ##

Information for building gferasure can be found in doc/build.md or doc/INSTALL

## Additional Information ##

Technical Report: To be published in near future

### Contribution guidelines ###

We are happy to expand Gferasure, to contribute please fork and submit a pull
request for review.

### Nightly Build Tests ###

We host an archive of our [nightly tests](http://galois.soe.ucsc.edu/Archives) at is updated nightly at 2am Pacific.

### Who do I talk to? ###

[Ethan Miller](http://www.ssrc.ucsc.edu/person/elm.html)

[Thomas Schwarz](http://www.ssrc.ucsc.edu/person/tschwarz.html)

[Lincoln Thurlow](http://www.ssrc.ucsc.edu/person/lthurlow.html)

[Andrew Kwong](http://www.ssrc.ucsc.edu/person/amkwong.html)
